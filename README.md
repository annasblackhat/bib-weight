### VERSION HISTORY
| Version | Total Datasets | Epoch | File | Notes |      
|---------|----------------|-------|------|-------|
| initial | 218 | 300 |  [bib-best.pt](bib-best.pt)|   |   
| 2nd | 1237 | 100 | [bib-best-v2.pt](bib-best-v2.pt)|   |  
